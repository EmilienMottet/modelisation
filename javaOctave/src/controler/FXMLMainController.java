/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import dk.ange.octave.OctaveEngine;
import dk.ange.octave.OctaveEngineFactory;
import dk.ange.octave.type.OctaveDouble;
import java.io.File;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.ImageTeste;

/**
 *
 * @author emilien
 */
public class FXMLMainController implements Initializable {

    @FXML
    private CheckMenuItem checkPropre;

    @FXML
    private CheckMenuItem checkImage;

    @FXML
    private CheckMenuItem checkHistogramme;

    @FXML
    private CheckMenuItem checkChi;

    @FXML
    private HBox boxCenter;

    @FXML
    private ImageView imageView;

    @FXML
    private ImageView imageViewComp;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonStart;

    @FXML
    private Button buttonStop;

    @FXML
    private MenuItem menuItemAdd;

    @FXML
    private HBox hBoxResultat;

    @FXML
    private Spinner<Integer> nbResult;

    @FXML
    private ProgressBar progressBar;
    
    private Service<Void> recherche;
    private List<ImageTeste> listImageTeste = new LinkedList<>();
    //equivalent de la console d'octave
    private OctaveEngine octave = new OctaveEngineFactory().getScriptEngine();
    private File dataBase = new File("./Projet/Visages");
    private File imageAtest;
    private final BooleanProperty estpret = new SimpleBooleanProperty(false);

    /**
     * permet d'activer la méthode désirée
     *
     * @param event
     */
    @FXML
    private void selectChi(ActionEvent event) {
        checkHistogramme.setSelected(false);
        checkImage.setSelected(false);
        checkPropre.setSelected(false);
    }

    @FXML
    private void selectHistogramme(ActionEvent event) {
        checkImage.setSelected(false);
        checkPropre.setSelected(false);
        checkChi.setSelected(false);
    }

    @FXML
    private void selectPropre(ActionEvent event) {
        checkImage.setSelected(false);
        checkHistogramme.setSelected(false);
        checkChi.setSelected(false);
    }

    @FXML
    private void selectImage(ActionEvent event) {
        checkPropre.setSelected(false);
        checkHistogramme.setSelected(false);
        checkChi.setSelected(false);
    }

    /**
     * ajout d'une image à la bd si besoin on la reconditionne
     * et on la write dans le
     * dossier de la bd et on fait pop un dialog box pour avertir l'ajout
     *
     * le temps d'execution est assez long car on doit recalculer M et P
     * 
     * @param event
     */
    @FXML
    private void addDb(ActionEvent event) {
        File selectfile = myFileChooser();
        if (selectfile != null) {
            if (selectfile.canRead()) {
                octave.eval("I = uint8(double(imread(\"" + selectfile.getAbsolutePath() + "\")));");
                octave.eval("J = reconditionnerImg(I);");
                String nom = dataBase.getAbsolutePath() + "/test" + (dataBase.listFiles().length + 1);
                octave.eval("imwrite(J,\"" + nom + ".png\",\"png\");");
                execFunctionOctave();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ajout d'image à la base de donnée");
                alert.setHeaderText("L'image a bien été ajouté à la base de donnée");
                alert.showAndWait();
            }
        }
    }

    /**
     * simpe file chooser avec avec trois filtres ( gif, png-jpg-pgm-jpeg, tous
     * les fichier )
     *
     * @return
     */
    private File myFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choississez une portrait à tester");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Portrait au format gif (en noir et blanc)", "*.gif"),
                new FileChooser.ExtensionFilter("Portrait au format png,jpg,pgm ", "*.png", "*.jpg", "*.pgm", "*.jpeg"),
                new FileChooser.ExtensionFilter("tous les fichiers ( déconseiller ! )", "*")
        );
        return fileChooser.showOpenDialog(new Stage());
    }

    /**
     * récupère l'image inconnue et débloque les actions pour lancer la recherche
     *
     * @param event
     */
    @FXML
    private void openImage(ActionEvent event) {
        File selectedFile = myFileChooser();
        if (selectedFile != null) {
            if (selectedFile.canRead()) {
                imageAtest = selectedFile;
                imageView.setImage(new Image(selectedFile.toURI().toString()));
                estpret.set(true);
            }
        }
    }

    /**
     * démarre le programme de recherche d'image, on charge l'image inconnue et
     * après on lance dans un thread le parcours de bd 
     * et on fait une dernieèe actualisation du top
     *
     * @param event
     */
    @FXML
    private void startProgram(ActionEvent event) {

        listImageTeste = new LinkedList<>();
        octave.eval("J = imresize(uint8(double(imread(\"" + imageAtest.getAbsolutePath() + "\"))),[128 128]);");
        octave.eval("I = reconditionnerImg(J);");
        if (checkPropre.isSelected()) {
            octave.eval("VS =I2Vector(I) ;");
            calculerVPS();
        }
        hBoxResultat.getChildren().clear();
        recherche = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        Platform.runLater(() -> {
                            buttonStart.setText("Recherche en cours");
                            buttonStart.setOnAction(null);
                            buttonAdd.setDisable(true);
                        });
                        parcourBd();
                        afficherTop();
                        Platform.runLater(() -> {
                            buttonStart.setText("Lancer recherche");
                            buttonAdd.setDisable(false);
                            buttonStart.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    startProgram(event);
                                }
                            });
                        });
                        return null;
                    }
                };
            }
        };

        recherche.start();

    }


    /**
     * fonction qui idéalement met en pause la recherche, on arrive à la mettre
     * en pause mais si on veut la reprendre on doit la restart obligatoirement
     * ... Dans l'état actuel elle quitte juste l'application
     *
     * @param event
     */
    @FXML
    private void stopProgram(ActionEvent event) {
        //tentative de stop le thread pour le reprendre mais resultat pas assez concluant
/*        if(buttonStop.getText().equals("Stop")){
            try {
                buttonStop.setText("Reprendre");
                recherche.cancel();        
            } catch (Exception e) {
            }

        }else{
            try {
                buttonStop.setText("Stop");
                recherche.restart();        
            } catch (Exception e) {
            }
        }*/
        Platform.exit();
    }

    public OctaveEngine getOctave() {
        return octave;
    }


    public boolean isEstpret() {
        return estpret.get();
    }

    public void setEstpret(boolean value) {
        estpret.set(value);
    }

    public BooleanProperty estpretProperty() {
        return estpret;
    }

    /**
     * parcours la bd à la recherche d'image proche. On compare l'image i de la bd
     * avec l'image inconnue. Ce test retourne une différence, plus cette valeur est
     * proche de zero plus l'image est ressemblante. On stocke cette nouvelle
     * valeur dans un object ImageTeste qui contient aussi un object File de l'image
     */
    private void parcourBd() {
        int i = 1;
        progressBar.setVisible(true);
        for (File listFile : dataBase.listFiles()) {
            String ext = getFileExtension(listFile);
            if (ext.equals("gif") || ext.equals("png")) {
                progressBar.setProgress(new Float(i) / dataBase.list().length);
                octave.eval("i = " + i + ";");
                i++;
                changerImageEnTest(listFile);
                octave.eval("J = uint8(double(imread(\"" + listFile.getAbsolutePath() + "\")));");
                appliquerAlgo();
                OctaveDouble res = (OctaveDouble) octave.get("D");
                listImageTeste.add(new ImageTeste(listFile, res.getData()[0]));
                afficherTop();
            }
        }
        imageViewComp.setImage(null);
        progressBar.setVisible(false);
    }

    /**
     *
     * @param file
     * @return l'extenstion du fichier sour forme de String ( sans le . )
     */
    private String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * iniatilisation de l'application
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        progressBar.setVisible(false);
        nbResult.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, dataBase.listFiles().length, 3));
        buttonStart.disableProperty().bind(estpretProperty().not());
        buttonStop.disableProperty().bind(estpretProperty().not());
        checkImage.setSelected(true);
        chargerOctaveForge();
        execFunctionOctave();
    }

    private void execFunctionOctave() {
        // function 
        /**
         * Créer une matrice vide, permettant de contenir sous forme
         * de vecteurs toutes les images de la bd
         */

        octave.eval("function M=creerM(taille)\n"
                + "  M=zeros(128*128,taille);\n"
                + "endfunction");

        // function distance(methode vecteur des images)
        /**
         * retourne la difference entre 2 matrice, pour faire ca, les
         * transforme en vecteur, et calcule la normale de la différence des
         * deux vecteur
         */
        octave.eval("function D=distance(I,J)\n"
                + "  V1=I2Vector(I);\n"
                + "  V2=I2Vector(J);\n"
                + "  D=norm(V1-V2);\n"
                + "endfunction");

        // function I2Vector
        /**
         * transforme une matrice en vecteur
         */
        octave.eval("function V=I2Vector(I)\n"
                + "  [N,L]=size(I);\n"
                + "  V=zeros(N*N,1);\n"
                + "  for i=1:N\n"
                + "    V(((i*N)-N+1):(i*N),1)=(I(i,:)');\n"
                + "  end\n"
                + "endfunction");

        // function Vector2I
        /**
         * transforme un vecteur en matrice
         */
        octave.eval("function J=Vector2I(V)\n"
                + "  [N,L]=size(V);\n"
                + "  N=sqrt(N);\n"
                + "  J=zeros(N);\n"
                + "  for i=1:N\n"
                + "   J(i,:) = V(((i*N)-N+1):(i*N),1)';\n"
                + "  end\n"
                + "endfunction");

        // function RGB2Gray(I)
        /**
         * transforme une image couleur en image en nuance de gris. Pour une
         * image en couleur, un pixel a trois valeur(Red,Green, Blue), ces trois
         * valeurs vont de 0 à 255, pour une image en nuance de gris, un pixel a
         * une valeur qui represente une nuance codée de 0 à 255 ( 0 blanc, 1-254
         * melange de noir et blanc ,255 noir) donc, on fait la moyenne des 3
         * valeur, pour qu'un pixel n'ait qu'une valeur
         */
        octave.eval("function J = RGB2Gray(I)\n"
                + "    [L,N,C]=size(I);\n"
                + "    J=zeros(L,N);\n"
                + "    J= (1/3)*(I(:,:,1)+I(:,:,2)+I(:,:,3));\n"
                + "end");

        // on charge une marice M, cette matrice contient toutes les images sous forme de vecteurs 
        chargerM();
        //on calcule les visages propres 
        calculerLesVP();
        //on calcule les vecteurs propres pour chaque image de la bd
        calculerP();

        // comparaison histogramme pas très efficace (à ne pas prendre en compte ...)
        octave.eval("function D = maCompHist(I,J)\n"
                + "  [A,b] = imhist (I);\n"
                + "  [B,b] = imhist (J);\n"
                + "  D = 0;\n"
                + "  len = min(rows(A),rows(B));\n"
                + "  for i=1:len\n"
                + "    D = D + abs(A(i,1)-B(i,1));\n"
                + "  endfor\n"
                + "endfunction");

        //chi-squared distance
        /**
         * d(x,y) = sum((xi-yi)^2/ (xi+yi))
         */
        octave.eval("function D = chisquared( I, J )\n"
                + "  [X,b] = imhist (I);\n"
                + "  [Y,b] = imhist (J);\n"
                + "  %d(x,y) = sum((xi-yi)^2/ (xi+yi)) /2;\n"
                + "  D=norm((X(:,1)-Y(:,1)).*(X(:,1)-Y(:,1))/(X(:,1)+Y(:,1))) /2;\n"
                + "end");
        // fonction transformant une image pour qu'elle soit sauvegardable dans la bd
        /**
         * on resize l'image en 128*128 pour qu'elle soit comme les autres
         * images de la bd si elle a 3 dimensions. C'est qu'elle est en couleur
         * donc on la transforme en noir et blanc maintenant : elle a un format
         * valide pour être écrite dans la bd
         */
        octave.eval("function J = reconditionnerImg(I)\n"
                + "  J = imresize(I,[128 128]);\n"
                + "  if(  isgray(J) == 0)\n"
                + "    J = RGB2Gray(J);\n"
                + "  endif\n"
                + "endfunction");
    }

    /**
     * on parcourt la base de données on transforme toutes les images en vecteurs
     * et on les stocke dans M
     */
    private void chargerM() {
        octave.eval("taille=" + dataBase.listFiles().length + ";");
        octave.eval("M=creerM(taille);");
        int i = 1;
        for (File listFile : dataBase.listFiles()) {
            if (getFileExtension(listFile).equals("gif") || getFileExtension(listFile).equals("png")) {
                octave.eval("J = uint8(double(imread(\"" + listFile.getAbsolutePath() + "\")));");
                octave.eval("J=I2Vector(J);");
                octave.eval("M(:," + i + ")=J;");
                i++;
            }
        }
    }

    /**
     * on calcule 5 visages propres, On considere que 5 est un bon compromis pour
     * avoir un début de reconstruction du visage (fait ressortir l'essentiel des traits importants du visage)
     * et un bon compromis en temps d'exécution
     */
    private void calculerLesVP() {
        octave.eval("[u,s,v]=svd(M,'econ');\n"
                + "VP1 = u(:,1);\n"
                + "VP2 = u(:,2);\n"
                + "VP3 = u(:,3);\n"
                + "VP4 = u(:,4);\n"
                + "VP5 = u(:,5);\n");
    }

    /**
     * on reconstruit tous les visages de la bd avec les 5 visages propres
     * calculer précédemment
     */
    private void calculerP() {
        octave.eval("P = creerM(taille);\n"
                + "for i=1:taille\n"
                + "  P1 = ( M(:,i)' * VP1 )*VP1;\n"
                + "  P2 = ( M(:,i)' * VP2 )*VP2;\n"
                + "  P3 = ( M(:,i)' * VP3 )*VP3;\n"
                + "  P4 = ( M(:,i)' * VP4 )*VP4;\n"
                + "  P5 = ( M(:,i)' * VP5 )*VP5;\n"
                + "  P(:,i)= P1 + P2 + P3 +P4 +P5;\n"
                + "end");
    }

    /**
     * reconstruction du visage Sujet ( l'inconnu )
     */
    private void calculerVPS() {
        octave.eval("PS1 =  (VS' * VP1 )* VP1;\n"
                + "PS2 = ( VS' * VP2 )* VP2;\n"
                + "PS3 = ( VS' * VP3 )* VP3;\n"
                + "PS4 = ( VS' * VP4 )* VP4;\n"
                + "PS5 = ( VS' * VP5 )* VP5;\n"
                + "PS= PS1 +PS2 +PS3 +PS4 +PS5;\n");
    }

    /**
     * fonction permettant d'afficher les N plus proches images de l'image inconnue
     */
    private void afficherTop() {
        listImageTeste.sort((im1, im2) -> {
            return (int) (im1.getDif() - im2.getDif());
        });
        Platform.runLater(() -> {
            try {
                hBoxResultat.getChildren().clear();
                for (int i = 0; i < nbResult.getValue() && i < listImageTeste.size(); i++) {
                    //peut etre utilisé set au lieu de clear en haut
                    hBoxResultat.getChildren().add(new ImageView(new Image(listImageTeste.get(i).getFile().toURI().toString())));
                }
            } catch (Exception e) {

            }
        });

    }

    /**
     * simple fonction permmettant de changer l'image qui est en cours de test
     *
     * @param listFile
     */
    private void changerImageEnTest(File listFile) {
        imageViewComp.setImage(new Image(listFile.toURI().toString()));
    }

    /**
     * Applique un algo de reconnaissance faciale choisi préalablement doit
     * obligatoirement iniatilisé D
     */
    private void appliquerAlgo() {

        if (checkImage.isSelected()) {
            // on pourrait utilisé M ( et ne pas appeller la fonction distance ) pour gagner en temps d'exectution ( on n'as pas a recalculer le Vecteur de l'image de la bd )
            octave.eval("D = distance(I,J);");
            return;
        }
        if (checkPropre.isSelected()) {
            // on compare la reconstruction de l'image inconnu avec les autres reconstructions des images contenu dans la base de donnée
            octave.eval("D = norm(P(:,i)-(PS));");
            return;
        }
        // inneficace comme methode
        if (checkHistogramme.isSelected()) {
            octave.eval("D = maCompHist(I,J);");
        }
        if (checkChi.isSelected()) {
            octave.eval("D = chisquared( I, J );");
        }
    }

    private void chargerOctaveForge() {
        // on ajoute au path le dossier ou est contenu la librairie
        octave.eval("addpath(\"./Projet/inst\")");
    }
}