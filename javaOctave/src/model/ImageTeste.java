/*
 * Cette classe est composé d'un File ( path de l'image principalement )
 * et d'une différence ( score qui evalue les similitudes entre deux images )
 */
package model;

import java.io.File;

/**
 *
 * @author emilien
 */
public class ImageTeste {
    private File file;
    private double dif;

    public File getFile() {
        return file;
    }

    public ImageTeste(File file, double dif) {
        this.file = file;
        this.dif = dif;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public double getDif() {
        return dif;
    }

    public void setDif(double dif) {
        this.dif = dif;
    }

    @Override
    public String toString() {
        return "ImageTeste{" + "file=" + file + ", dif=" + dif + '}';
    }
    
    
}