/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package launcher;

import controler.FXMLMainController;
import dk.ange.octave.OctaveEngine;
import dk.ange.octave.OctaveEngineFactory;
import dk.ange.octave.type.OctaveDouble;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author emilien
 */
public class JavaOctave extends Application {
    
    private FXMLMainController fXMLMainController;
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmll = new FXMLLoader(getClass().getResource("/view/FXMLMain.fxml"));
        Parent root = fxmll.load();
        fXMLMainController = fxmll.getController();
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        
    }

    /**
     * quitte octave en même temps que l'on quitte l'application
     * @throws Exception 
     */
    @Override
    public void stop() throws Exception {
        super.stop(); 
        fXMLMainController.getOctave().close();
    }
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        
    }
    
    
}
