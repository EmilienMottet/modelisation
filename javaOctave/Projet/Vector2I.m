function J=Vector2I(V)
  [N,L]=size(V);
  N=sqrt(N);
  J=zeros(N);
  for i=1:N
   J(i,:) = V(((i*N)-N+1):(i*N),1)';
  end
endfunction