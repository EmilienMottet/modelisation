function D=distance(I,J)
  V1=I2Vector(I);
  V2=I2Vector(J);
  D=norm(V1-V2);
endfunction
  