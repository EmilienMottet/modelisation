function V=I2Vector(I)
  [N,L]=size(I);
  V=zeros(N*N,1);
  for i=1:N
    V(((i*N)-N+1):(i*N),1)=(I(i,:)');
  end
endfunction