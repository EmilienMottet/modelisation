#Ex6
[u,s,v]=svd(M,'econ');
VP1 = u(:,1);
VP2 = u(:,2);
VP3 = u(:,3);
VP4 = u(:,4);
VP5 = u(:,5);

V = M(:,20); % 20 choisit arbritrairement

P1 = (V'*VP1)*VP1;

I=Vector2I(P1);
figure();
imshow(uint8( I));

P2 = (V'*VP2)*VP2;
P3 = (V'*VP3)*VP3;
P4 = (V'*VP4)*VP4;
P5 = (V'*VP5)*VP5;

figure();
imshow(uint8(Vector2I(P1+P2)));

figure();
imshow(uint8(Vector2I(P1+P2+P3)));


figure();
imshow(uint8(Vector2I(P1+P2+P3+P4)));


figure();
imshow(uint8(Vector2I(P1+P2+P3+P4+P5)));