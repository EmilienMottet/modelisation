clear all
close all

#Ex4

M = zeros(16384,55);

for i=1:55
  I=double(imread(strcat(strcat("./Visages/test",int2str(i)),".gif")));
  J=I2Vector(I);
  M(:,i)=J;
end 

#Ex5
i=56;
VS=I2Vector( double(imread(strcat(strcat("./Visages/test",int2str(i)),".gif"))));
Dif=zeros(16384,55);
Nor = zeros(1,55);
for i=1:55
  Dif(:,i)=M(:,i)-VS;
  Nor(1,i) = norm(Dif(:,i));
end

[N,P] = sort(Nor(1,:));

I=double(imread(strcat(strcat("./Visages/test",int2str(P(1))),".gif")));
figure();
imshow(uint8(I));

#Ex6
[u,s,v]=svd(M,'econ');
VP1 = u(:,1);
VP2 = u(:,2);
VP3 = u(:,3);
VP4 = u(:,4);
VP5 = u(:,5);

V = M(:,20); % 20 choisit arbritrairement

P1 = (V'*VP1)*VP1;

I=Vector2I(P1);
figure();
imshow(uint8( I));

P2 = (V'*VP2)*VP2;
P3 = (V'*VP3)*VP3;
P4 = (V'*VP4)*VP4;
P5 = (V'*VP5)*VP5;

%figure();
%imshow(uint8(Vector2I(P1+P2)));
%
%figure();
%imshow(uint8(Vector2I(P1+P2+P3)));
%
%figure();
%imshow(uint8(Vector2I(P1+P2+P3+P4)));
%
%
%figure();
%imshow(uint8(Vector2I(P1+P2+P3+P4+P5)));

#Ex7

P = zeros(16384,55);
for i=1:55
  P1 = ( M(:,i)' * VP1 )*VP1;
  P2 = ( M(:,i)' * VP2 )*VP2;
  P3 = ( M(:,i)' * VP3 )*VP3;
  P4 = ( M(:,i)' * VP4 )*VP4;
  P5 = ( M(:,i)' * VP5 )*VP5;
  P(:,i)= P1 + P2 + P3 +P4 +P5;
end

PS1 =  (VS' * VP1 )* VP1;
PS2 = ( VS' * VP2 )* VP2;
PS3 = ( VS' * VP3 )* VP3;
PS4 = ( VS' * VP4 )* VP4;
PS5 = ( VS' * VP5 )* VP5;

PS= PS1 +PS2 +PS3 +PS4 +PS5;

Dif=zeros(16384,55);
Nor = zeros(1,55);
for i=1:55
  Dif(:,i)=P(:,i)-(PS1+PS2+PS3+PS4+PS5);
  Nor(1,i) = norm(Dif(:,i));
end 

[N,P] = sort(Nor(1,:));

I=double(imread(strcat(strcat("./Visages/test",int2str(P(1))),".gif")));
figure();
imshow(uint8(I));

