function J = imgASave(I)
  J = imresize(I,[128 128]);
  if(  isgray(J) == 0)
    J = RGB2Gray(J);
  endif
endfunction