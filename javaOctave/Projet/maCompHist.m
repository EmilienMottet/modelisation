function D = maCompHist(I,J)
  [A,b] = imhist (I);
  [B,b] = imhist (J);
  D = 0;
  len = min(rows(A),rows(B));
  for i=1:len
    D = D + abs(A(i,1)-B(i,1));
  endfor
endfunction