function D = chisquared( X, Y )
  %d(x,y) = sum((xi-yi)^2/ (xi+yi)) /2;
  D=norm((X(:,1)-Y(:,1)).*(X(:,1)-Y(:,1))/(X(:,1)+Y(:,1))) /2;
end